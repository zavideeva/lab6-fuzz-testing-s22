import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

describe('Bonus system tests', () => {
  let app;
  console.log("Tests started");

  test('Standard < 10000 test',  (done) => {
    let program = "Standard";
    let amount = 9999;
    expect(calculateBonuses(program, amount)).toEqual(1*0.05);
    done();
  });

  test('Standard = 10000 test',  (done) => {
    let program = "Standard";
    let amount = 10000;
    expect(calculateBonuses(program, amount)).toEqual(1.5*0.05);
    done();
  });

  test('Standard < 50000 test',  (done) => {
    let program = "Standard";
    let amount = 49999;
    expect(calculateBonuses(program, amount)).toEqual(1.5*0.05);
    done();
  });

  test('Standard < 50000 test',  (done) => {
    let program = "Standard";
    let amount = 49999;
    expect(calculateBonuses(program, amount)).toEqual(1.5*0.05);
    done();
  });

  test('Standard = 50000 test',  (done) => {
    let program = "Standard";
    let amount = 50000;
    expect(calculateBonuses(program, amount)).toEqual(2*0.05);
    done();
  });

  test('Standard < 100000 test',  (done) => {
    let program = "Standard";
    let amount = 99999;
    expect(calculateBonuses(program, amount)).toEqual(2*0.05);
    done();
  });

  test('Standard = 100000 test',  (done) => {
    let program = "Standard";
    let amount = 100000;
    expect(calculateBonuses(program, amount)).toEqual(2.5*0.05);
    done();
  });

  test('Standard > 100000 test',  (done) => {
    let program = "Standard";
    let amount = 100001;
    expect(calculateBonuses(program, amount)).toEqual(2.5*0.05);
    done();
  });

  test('Premium < 10000 test',  (done) => {
    let program = "Premium";
    let amount = 999;
    expect(calculateBonuses(program, amount)).toEqual(1*0.1);
    done();
  });

  test('Diamond < 10000 test',  (done) => {
    let program = "Diamond";
    let amount = 999;
    expect(calculateBonuses(program, amount)).toEqual(1*0.2);
    done();
  });

  test('LoL < 10000 test',  (done) => {
    let program = "LoL";
    let amount = 999;
    expect(calculateBonuses(program, amount)).toEqual(0*0.2);
    done();
  });

  console.log('Tests Finished');
})